package com.example.zinier.utils;

public class Constants {

    public static final String BASE_URL_POSTER = "https://image.tmdb.org/t/p/";
    public static final String POSTER_SIZE_SMALL = "w200/";
    public static final String POSTER_SIZE_MEDIUM = "w300/";
    public static final String POPULAR_MOVIES_BASE_URL = "https://api.themoviedb.org/3/movie/popular/";
    public static final String SEARCH_MOVIES_BASE_URL = "https://api.themoviedb.org/3/search/movie/";
    public static final String API_KEY = "541706bcef5358f31be35a7ab6ca1a33";

}
