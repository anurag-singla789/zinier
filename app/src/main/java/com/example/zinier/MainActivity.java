package com.example.zinier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.example.zinier.pojo.MovieInfo;
import com.example.zinier.pojo.MoviesResponseInfo;
import com.example.zinier.retrofit.MovieInterface;
import com.example.zinier.utils.CommonUtils;
import com.example.zinier.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements MoviesAdapter.ItemClickListener {

    private RecyclerView mMoviesRecyclerView;
    private ProgressBar mProgressBar;
    private EditText mSearchMoviesEditText;

    private List<MovieInfo> mMoviesInfoList = new ArrayList<>();
    private List<MovieInfo> mSearchInfoList = new ArrayList<>();
    private MoviesAdapter mMoviesAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private boolean isLoading;
    private boolean isLastPage;
    private int mPageNo = 1;
    private int mSearchPageNo = 1;

    public static final String KEY_INTENT_MOVIE_INFO = "KEY_INTENT_MOVIE_INFO";
    private static final int NUMBER_OF_COLUMNS = 3;

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItemPosition = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    if(mSearchInfoList != null && mSearchInfoList.size() > 0) {
                       searchMovies(mSearchMoviesEditText.getText().toString());
                    }else{
                        getMovies();
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
    }

    private void initUI(){
        mMoviesRecyclerView = findViewById(R.id.recyclerview_movies);
        mProgressBar = findViewById(R.id.progressBar);
        mSearchMoviesEditText = findViewById(R.id.edittext_search_movies);

        mLayoutManager = new GridLayoutManager(this, NUMBER_OF_COLUMNS);
        mMoviesRecyclerView.setLayoutManager(mLayoutManager);
        mMoviesAdapter = new MoviesAdapter(this, mMoviesInfoList);
        mMoviesAdapter.setmClickListener(this);
        mMoviesRecyclerView.setAdapter(mMoviesAdapter);
        mMoviesRecyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        mSearchMoviesEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                 if(s.length() > 1 && s.length() %  2 == 0){
                     mSearchInfoList.clear();
                     mSearchPageNo = 1;
                     searchMovies(s.toString());
                 }else if(s.length() == 0){
                     mMoviesAdapter.setmMovieInfoList(mMoviesInfoList);
                     mMoviesAdapter.notifyDataSetChanged();
                 }

            }
        });


        getMovies();
    }

    @Override
    public void onItemClick(View view, int position, MovieInfo movieInfo) {
         Intent movieDetailsIntent = new Intent(MainActivity.this, MovieDetailsActivity.class);
         movieDetailsIntent.putExtra(KEY_INTENT_MOVIE_INFO, movieInfo);
         startActivity(movieDetailsIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) item.getActionView();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_list_item_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<MovieInfo> movieInfoList;
                if(mSearchInfoList != null && mSearchInfoList.size() > 0){
                    movieInfoList = mSearchInfoList;
                }else{
                    movieInfoList = mMoviesInfoList;
                }

                if(position == 0){
                    Collections.sort(movieInfoList, new Comparator<MovieInfo>(){
                        @Override
                        public int compare(MovieInfo o1, MovieInfo o2) {
                            return Float.valueOf(o2.popularity).compareTo(Float.valueOf(o1.popularity));// To compare string values
                        }
                    });
                    mMoviesAdapter.setmMovieInfoList(movieInfoList);
                    mMoviesAdapter.notifyDataSetChanged();
                }else if(position == 1){
                    Collections.sort(movieInfoList, new Comparator<MovieInfo>(){
                        @Override
                        public int compare(MovieInfo o1, MovieInfo o2) {
                            return Float.valueOf(o2.voteAverage).compareTo(Float.valueOf(o1.voteAverage));// To compare string values
                        }
                    });
                    mMoviesAdapter.setmMovieInfoList(movieInfoList);
                    mMoviesAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setAdapter(adapter);
        return true;
    }


    private void getMovies(){
        isLoading = true;
        mProgressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.POPULAR_MOVIES_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                 .build();
        MovieInterface service = retrofit.create(MovieInterface.class);
        Call<MoviesResponseInfo> call = service.listPopularMovies(Constants.API_KEY, "en-US", mPageNo);
        call.enqueue(new Callback<MoviesResponseInfo>() {
            @Override
            public void onResponse(Call<MoviesResponseInfo> call, Response<MoviesResponseInfo> response) {
                 List<MovieInfo> moviesList = response.body().results;
                 if(moviesList != null && moviesList.size() > 0){
                     mMoviesInfoList.addAll(moviesList);
                     mMoviesAdapter.setmMovieInfoList(mMoviesInfoList);
                     mMoviesAdapter.notifyDataSetChanged();
                     mSearchInfoList = new ArrayList<>();

                     if(response.body().totalPages > mPageNo){
                         mPageNo++;
                     }else{
                         isLastPage = true;
                     }
                 }

                 mProgressBar.setVisibility(View.GONE);
                 CommonUtils.hideKeyboard(MainActivity.this);
                 isLoading = false;
            }

            @Override
            public void onFailure(Call<MoviesResponseInfo> call, Throwable t) {
                 mProgressBar.setVisibility(View.GONE);
                 CommonUtils.hideKeyboard(MainActivity.this);
                 isLoading = false;
            }
        });
    }

    private void searchMovies(String query){
        isLoading = true;
        mProgressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SEARCH_MOVIES_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MovieInterface service = retrofit.create(MovieInterface.class);
        Call<MoviesResponseInfo> call = service.searchMovies(Constants.API_KEY, "en-US", query, mSearchPageNo);
        call.enqueue(new Callback<MoviesResponseInfo>() {
            @Override
            public void onResponse(Call<MoviesResponseInfo> call, Response<MoviesResponseInfo> response) {
                if(!mSearchMoviesEditText.getText().toString().isEmpty()) {
                    List<MovieInfo> moviesList = response.body().results;
                    if (moviesList != null && moviesList.size() > 0) {
                        mSearchInfoList.addAll(moviesList);
                        mMoviesAdapter.setmMovieInfoList(mSearchInfoList);
                        mMoviesAdapter.notifyDataSetChanged();

                        if(response.body().totalPages > mSearchPageNo){
                            mSearchPageNo++;
                        }else{
                            isLastPage = true;
                        }
                    }
                }else{
                    mSearchInfoList = new ArrayList<>();
                    mSearchPageNo = 1;
                }
                mProgressBar.setVisibility(View.GONE);
                CommonUtils.hideKeyboard(MainActivity.this);
                isLoading = false;
            }

            @Override
            public void onFailure(Call<MoviesResponseInfo> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                CommonUtils.hideKeyboard(MainActivity.this);
                isLoading = false;
            }
        });
    }
}
