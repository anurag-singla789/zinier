package com.example.zinier;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zinier.pojo.MovieInfo;
import com.example.zinier.utils.Constants;
import com.squareup.picasso.Picasso;

public class MovieDetailsActivity extends AppCompatActivity {

    private TextView mOriginalTitleTextView, mOverviewTextView, mUserRatingTextView, mReleaseDateTextView;
    private ImageView mPosterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    private void initUI(){
        mOriginalTitleTextView = findViewById(R.id.textview_original_title);
        mOverviewTextView = findViewById(R.id.textview_overview);
        mUserRatingTextView = findViewById(R.id.textview_user_rating);
        mReleaseDateTextView = findViewById(R.id.textview_release_date);
        mPosterImageView = findViewById(R.id.imageview_poster);

        MovieInfo movieInfo = (MovieInfo)getIntent().getSerializableExtra(MainActivity.KEY_INTENT_MOVIE_INFO);
        mOriginalTitleTextView.setText(movieInfo.originalTitle);
        mOverviewTextView.setText(movieInfo.overview);
        mReleaseDateTextView.setText(getString(R.string.release_date, movieInfo.releaseDate));
        mUserRatingTextView.setText(getString(R.string.rating, movieInfo.voteAverage + ""));

        Picasso.get()
                .load(Constants.BASE_URL_POSTER + Constants.POSTER_SIZE_MEDIUM + movieInfo.posterPath)
                .resize(300, 300)
                .into(mPosterImageView);
    }

}
