package com.example.zinier.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MoviesResponseInfo {
    @SerializedName("page")
    public int page;

    @SerializedName("total_results")
    public long totalResults;

    @SerializedName("total_pages")
    public long totalPages;

    @SerializedName("results")
    public List<MovieInfo> results;

}
