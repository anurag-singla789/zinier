package com.example.zinier.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MovieInfo implements Serializable {
     @SerializedName("vid_count")
     public long vidCount;

     @SerializedName("id")
     public long id;

     @SerializedName("video")
     public boolean video;

     @SerializedName("vote_average")
     public float voteAverage;

     @SerializedName("title")
     public String title;

     @SerializedName("popularity")
     public float popularity;

     @SerializedName("poster_path")
     public String posterPath;

     @SerializedName("original_language")
     public String originalLanguage;

     @SerializedName("original_title")
     public String originalTitle;

     @SerializedName("backdrop_path")
     public String backdropPath;

     @SerializedName("overview")
     public String overview;

     @SerializedName("release_date")
     public  String releaseDate;


}
