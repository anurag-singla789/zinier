package com.example.zinier;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zinier.pojo.MovieInfo;
import com.example.zinier.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {


    private List<MovieInfo> mMovieInfoList;
    private LayoutInflater mInflater;

    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    private ItemClickListener mClickListener;

    // data is passed into the constructor
    MoviesAdapter(Context context, List<MovieInfo> movieInfoList) {
        this.mInflater = LayoutInflater.from(context);
        this.mMovieInfoList = movieInfoList;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_movie_item, parent, false);
        return new ViewHolder(view);
    }

    public void setmMovieInfoList(List<MovieInfo> mMovieInfoList) {
        this.mMovieInfoList = mMovieInfoList;
    }


    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //https://image.tmdb.org/t/p/w200//xvx4Yhf0DVH8G4LzNISpMfFBDy2.jpg
        holder.mNameTextView.setText(mMovieInfoList.get(position).originalTitle);
        Picasso.get()
                .load(Constants.BASE_URL_POSTER + Constants.POSTER_SIZE_SMALL + mMovieInfoList.get(position).posterPath)
                .resize(120, 120)
                .into(holder.mPosterImageView);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mMovieInfoList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mNameTextView;
        ImageView mPosterImageView;

        ViewHolder(View itemView) {
            super(itemView);
            mNameTextView = itemView.findViewById(R.id.textview_name);
            mPosterImageView = itemView.findViewById(R.id.imageview_poster);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition(), mMovieInfoList.get(getAdapterPosition()));
        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, MovieInfo movieInfo);
    }
}
