package com.example.zinier.retrofit;

import com.example.zinier.pojo.MoviesResponseInfo;
import com.example.zinier.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieInterface {

    //https://api.themoviedb.org/3/movie/popular?api_key=541706bcef5358f31be35a7ab6ca1a33&language=en-US&page=1
    @GET(Constants.POPULAR_MOVIES_BASE_URL)
    Call<MoviesResponseInfo> listPopularMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);

    //https://api.themoviedb.org/3/search/movie?api_key=541706bcef5358f31be35a7ab6ca1a33&language=en-US&query=Cap&page=1&include_adult=false
    @GET(Constants.SEARCH_MOVIES_BASE_URL)
    Call<MoviesResponseInfo> searchMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("query") String query, @Query("page") int page);
}
